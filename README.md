# GitLab Google Cloud Platform Shared Runner Images

Images used by GitLab CI custom executor to run Jobs inside of Google Cloud Platform.

## Provisioning

To provision the images we are using
[chef-solo](https://www.packer.io/plugins/provisioners/chef/chef-solo). All
the cookbooks are listed inside of the [`cookbooks`](cookbooks) directory.

Some of the cookbooks are vendorded such as `chocolatey`, because
Packer simply uploads the cookbooks and doesn't run anything before the
specified `run_list`, taking a look at
[#6991](https://github.com/hashicorp/packer/issues/69920) and
[#590](https://github.com/hashicorp/packer/issues/590) vendoring the
cookbook is the simplest solution.

The last step of provisioning uses a community
[windows-update](https://github.com/rgl/packer-provisioner-windows-update)
provisioner that will install updates and reboot to finish applying them.

### Sync vendored cookbooks

```
make cookbook-vendor
```

### Container Image for CI Packer Builds

We are creating a custom container image here so that we can use a custom
provisioner for Windows updates. All the image has is the additional
provisioner that is expected to be in the same directory as the packer binary.

### Updating Chef

You can find the latest stable version of Chef for Windows via:

```bash
curl "https://omnitruck.chef.io/stable/chef/metadata?v=&p=windows&pv=2019&m=x86_64"
```

For example, this returns:

```plaintext
sha1	feef2b49c9f5a8334e475d4209bcc64fda3da228
sha256	a7505b392ce5888a885f5949fab7a4a6278825370ccbb8313b0cd03821eaf5a3
url	https://packages.chef.io/files/stable/chef/18.3.0/windows/2019/chef-client-18.3.0-1-x64.msi
```

Note that that the `pv` argument specifies the platform version (Windows 2019).

On Powershell, you can install the latest stable version for any Windows version via:

```powershell
. { Invoke-WebRequest -useb https://omnitruck.chef.io/install.ps1 } | Invoke-Expression; Get-ProjectMetadata -project chef -channel stable
```

You can also install this package via:

```powershell
. { Invoke-WebRequest -useb https://omnitruck.chef.io/install.ps1 } | Invoke-Expression; Install-Project -project chef -channel stable
```

In addition, you can look up older versions via the `v` parameter. For example, for `17.1.35`:

```bash
curl "https://omnitruck.chef.io/stable/chef/metadata?v=17.1.35&p=windows&pv=2019&m=x86_64"
```

`v` can also be shortened to major version, such as 17 or 18.
More details can be found in the [OmniTruck API documentation](https://docs.chef.io/api_omnitruck/).

### Value of `chef_solo_identifying_number`

We need the identifying number to uninstall `chef` from the machine
after it has provisioned the machine. To get the number install chef on
Windows machine and run `Get-WmiObject -Class Win32_Product -Filter
"vendor='Chef Software, Inc.'"` inside of Powershell. You can use the
machine that is running the CI job for this if you don't have access to
a Windows machine.
