default[:gitlab_runner] = {
  'root_path': 'C:\GitLab-Runner',
  'exe_path': 'C:\GitLab-Runner\gitlab-runner.exe',
  'version': 'v17.0.0',
  'checksum': '96d62a1f78c9ec2b508d2fd7a594ada52d78bdf3243350919a55c70ff09d531d',
}

default[:git] = {
  'root_path': 'C:\Git',
  'zip_path': 'C:\Git\mingit.zip',
  'version': '2.23.0',
  'build': '1',
  'checksum': '8f65208f92c0b4c3ae4c0cf02d4b5f6791d539cd1a07b2df62b7116467724735',
}

default[:git_lfs] = {
  'root_path': 'C:\GitLFS',
  'zip_path': 'C:\GitLFS\lfs.zip',
  'version': 'v2.8.0',
  'checksum': 'ffeb6e0a7d214155d87fde11c4366da4f7549705f93f8873ad43ec7520282d45',
}
