default[:visual_studio_build_tool] = {
  'version': '117.8.0.0',
  'desktop_workload_version': '1.0.0',
  'cpp_workload_version': '1.0.0',
  'web_workload_version': '1.0.0',
  'mobile_workload_version': '1.0.0',
}

default[:utils] = {
  'zip_version': '23.1.0',
  'wget_version': '1.21.4',
  'curl_version': '8.4.0',
  'jq_version': '1.7.0',
  'docker_compose_version': '2.23.1',
  'nuget_version': '6.8.0',
  'cmake_version': '3.27.8',
}

default[:docker] = {
  'docker_version': '20.10.24',
  'nuget_package_provider_version': '2.8.5.201',
}

default[:languages] = {
  'dotnetcore_version': '3.1.426',
  'ruby_version': '3.1.3.1',
  'go_version': '1.21.4',
  'nodejs_version': '21.2.0',
  'openjdk_version': '21.0.1',
}
