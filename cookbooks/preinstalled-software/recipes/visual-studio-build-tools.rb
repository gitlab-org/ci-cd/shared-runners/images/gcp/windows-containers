require_relative '../libraries/chocolatey'

# https://chocolatey.org/packages/visualstudio2022buildtools
chocolatey_package 'Install Visual Studio 2022 Build Tools' do
  package_name 'visualstudio2022buildtools'
  version node[:visual_studio_build_tool][:version]
  action :install
  # 3010 is returned by chocolatey for dotnetfx because it
  # requires a reboot to be fully installed. The reboot is handled
  # elsewhere.
  returns [0, 3010]
end

# https://chocolatey.org/packages/visualstudio2022-workload-manageddesktopbuildtools
chocolatey_package 'Install Visual Studio 2022 Build Tools .Net desktop workload' do
  package_name 'visualstudio2022-workload-manageddesktopbuildtools'
  version node[:visual_studio_build_tool][:desktop_workload_version]
  action :install
end

# https://chocolatey.org/packages/visualstudio2022-workload-vctools
chocolatey_package 'Install Visual Studio 2022 Build Tools C++ workload' do
  package_name 'visualstudio2022-workload-vctools'
  version node[:visual_studio_build_tool][:cpp_workload_version]
  action :install
end

# https://chocolatey.org/packages/visualstudio2022-workload-webbuildtools
chocolatey_package 'Install Visual Studio 2022 Build Tools Web development workload' do
  package_name 'visualstudio2022-workload-webbuildtools'
  version node[:visual_studio_build_tool][:web_workload_version]
  action :install
  # 3010 is returned by chocolatey for this build tool because it
  # requires a reboot to be fully installed. The reboot is handled
  # elsewhere.
  returns [0, 3010]
end

# https://chocolatey.org/packages/visualstudio2022-workload-xamarinbuildtools
chocolatey_package 'Install Visual Studio 2022 Build Tools Mobile development workload' do
  package_name 'visualstudio2022-workload-xamarinbuildtools'
  version node[:visual_studio_build_tool][:mobile_workload_version]
  action :install
  timeout 1800
end

