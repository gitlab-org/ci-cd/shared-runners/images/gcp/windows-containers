git 'vcpkg' do
    repository 'https://github.com/microsoft/vcpkg.git'
    checkout_branch '2023.01.09'
    destination 'C:\\vcpkg'
end

execute 'Install vcpkg' do
    command 'C:\\vcpkg\\bootstrap-vcpkg.bat'
end

execute 'Integrate vcpkg systemwide' do
    command 'C:\\vcpkg\\vcpkg integrate install'
    retries 3
end

windows_path 'C:\vcpkg' do
    action :add
end
