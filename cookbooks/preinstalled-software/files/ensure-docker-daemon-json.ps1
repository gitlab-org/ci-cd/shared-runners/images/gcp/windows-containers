
$dockerDataPath = "$( $env:ProgramData )\docker"
$dockerServiceName = "docker"
$dockerConfigPath = Join-Path $dockerDataPath "config"
$daemonSettingsFile = Join-Path $dockerConfigPath "daemon.json"

try {
    if (!(Test-Path "$dockerConfigPath")) {
        md -Path "$dockerConfigPath" | Out-Null
    }

    $daemonSettings = New-Object PSObject
    if ((Test-Path "$daemonSettingsFile")) {
        Write-Output "$daemonSettingsFile exists, reading it's content"

        Get-Content "$daemonSettingsFile"

        $daemonSettings = Get-Content "$daemonSettingsFile" | ConvertFrom-Json
    }

    # Add hard-coded DNS settings to work around DNS not working inside container
    # Executed only if dns property is not yet set in the object read from the existing
    # daemon.json file
    if (!($daemonSettings | Get-Member dns)) {
        $daemonSettings | Add-Member NoteProperty dns @("8.8.8.8", "8.8.4.4")
    }

    # Set hosts entry if not present
    if (!($daemonSettings | Get-Member hosts)) {
        $daemonSettings | Add-Member NoteProperty hosts @("npipe://")
    }

    $daemonSettings | ConvertTo-Json | Out-File -FilePath "$daemonSettingsFile" -Encoding ASCII

    Stop-Service -Name $dockerServiceName
    Start-Service -Name $dockerServiceName

    Get-Content "$daemonSettingsFile"
}
catch {
    Write-Error $_
}
