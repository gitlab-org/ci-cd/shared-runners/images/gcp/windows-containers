#######################
# Version definitions #
#######################

PACKER_VERSION ?= 1.5.1
WINDOWS_UPDATE_PROVISIONER_VERSION ?= 0.9.0

#####################
# CI Registry setup #
#####################

CI_REGISTRY ?= registry.gitlab.com
CI_PROJECT_PATH ?= gitlab-org/ci-cd/shared-runners/images/gcp/windows-containers
CI_REGISTRY_IMAGE ?= $(CI_REGISTRY)/$(CI_PROJECT_PATH)
CI_IMAGE ?= $(CI_REGISTRY_IMAGE)/ci:latest

.PHONY: build-ci-image
build-ci-image: IMAGE ?= $(CI_IMAGE)
build-ci-image:
ifdef CI_REGISTRY_USER
	# Logging into $(CI_REGISTRY)
	@docker login --username $(CI_REGISTRY_USER) --password $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	docker pull $(IMAGE) || echo "Remote image $(IMAGE) not available - will not use cache"
endif
	docker --debug build \
			--cache-from $(IMAGE) \
			--build-arg PACKER_VERSION=$(PACKER_VERSION) \
			--build-arg WINDOWS_UPDATE_PROVISIONER_VERSION=$(WINDOWS_UPDATE_PROVISIONER_VERSION) \
			-t $(IMAGE) \
			-f ./dockerfiles/ci/Dockerfile \
			./dockerfiles/ci/
ifdef CI_REGISTRY_USER
	# Pusing $(IMAGE)
	@docker push $(IMAGE)
	# Logging out from $(CI_REGISTRY)
	@docker logout $(CI_REGISTRY)
endif
